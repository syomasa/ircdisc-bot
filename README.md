# IRCtoDicord-bot

Small discord bot to forward messages from different IRC channels to discord and vice versa allowing seemless communication between discord and IRC

## Usage

1. Look at the [`env_example`](https://gitlab.com/syomasa/ircdisc-bot/-/blob/master/.env_example) and [`config_example.json`](https://gitlab.com/syomasa/ircdisc-bot/-/blob/master/config_template.json) for introductions how to create .env and config.json
2. run `python main.py`


