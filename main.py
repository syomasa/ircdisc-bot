import dotenv
import os
import re
import asyncio
import argparse
from threading import Thread
from discord.ext import commands, tasks
from modules import irc_listener, discord_sender

dotenv.load_dotenv()

PREFIX="-"

nick = os.getenv("NICK")
server = os.getenv("SERVER")
port = os.getenv("PORT")
ident = os.getenv("IDENT")
realname= os.getenv("REALNAME")
chan = os.getenv("CHAN")

bot = discord_sender.Server(command_prefix=PREFIX)
parser = argparse.ArgumentParser(description="Bot for connecting discord and IRC")
parser.add_argument("--proxy", "-p", action="store_true", help="Use proxy for connecting to irc server defined in .env")

args = parser.parse_args()


# Shared variables
buffer = None
guild_obj = None

if(args.proxy == True):
    irc_bot = irc_listener.Bot(server, nick, ident, realname, proxy=True)
else:
    irc_bot = irc_listener.Bot(server, nick, ident, realname)


@bot.event
async def on_ready():

    print("Bot is ready")
    bot.print_guilds()
    bot.set_irc_channels(irc_bot)
    irc_thread.start()
    send_buffer.start()

@bot.event
async def on_message(msg):
    guild = msg.guild
    channel = bot.get_channel_name(int(guild.id))

    if msg.content.startswith(PREFIX) or msg.author.bot == True:
        return

    attachments = msg.attachments

    if msg.author.nick != None and msg.channel.name == "general" and attachments:
        irc_bot.send(channel, f"<{msg.author.nick}> {attachments[0].proxy_url}")
        print(f"<{msg.author.nick}>", msg.content)
    elif msg.channel.name == "general" and attachments:
        irc_bot.send(channel, f"<{msg.author.name}> {attachments[0].proxy_url}")
        print(f"<{msg.author.name}>", msg.content)

    if msg.author.nick != None and msg.channel.name == "general":
        irc_bot.send(channel, f"<{msg.author.nick}> {msg.content}")
        print(f"<{msg.author.nick}>", msg.content)
    elif msg.channel.name == "general":
        irc_bot.send(channel, f"<{msg.author.name}> {msg.content}")
        print(f"<{msg.author.name}>", msg.content)

    await bot.process_commands(msg)

@tasks.loop(seconds=0.01)
async def send_buffer():
    global buffer
    global guild_obj

    if buffer != None:
        for channel in guild_obj.channels:
            if channel.name == "general":
                await channel.send(buffer)
                buffer = None

@bot.command(name="showguild")
async def guilds(ctx):
    print(bot.guilds)
    await ctx.send("Hello world")

@bot.command(name="testconfig")
async def testconfig(ctx):
    print(bot.config)

@bot.command(name="ircchannels")
async def ircchannels(ctx):
    bot.print_irc_channels()

@bot.command(name="updateconfig")
async def updateconfig(ctx):
    await ctx.send("Config updated")
    bot.update_config()

@irc_bot
def parse(ctx):
    global buffer
    global guild_obj

    while 1:
        try:
            msg = irc_bot.receive_msg()
            print(msg)

        except UnicodeDecodeError as e:
            print("Ignored following error", e)

        else:
            lines = msg.split("\r\n")
            for line in lines:
                parts = line.split(":")
                #print(parts[0].encode())
                if parts[0].strip() == "PING":
                    text = f"PONG :{parts[1]}\r\n"
                    ctx.s.send(text.encode())
                    print(f"Msg sent: {text}")
                else:
                    # I trust past me on this one :D
                    pattern =  re.match("(:.+?(?=!)).*?([A-Z].*)", line, re.M)
                    if pattern == None:
                        continue

                    groups = pattern.groups()
                    username = groups[0].strip(":")
                    split_command = groups[1].split(" ")
                    print(split_command)
                    #print(split_command[0:2])

                    try:
                        if("PRIVMSG" not in split_command[0:2]):
                            continue
                    except IndexError:
                           continue
                    else:
                        user_msg = ' '.join(split_command[2:])[1:]
                        print(f"<{username}> {user_msg}")
                        guild_id = bot.get_guild_id(split_command[1])
                        guild_obj = bot.get_guild(int(guild_id))
                        buffer = f"<{username}> {user_msg}"
                        print("Message goes to:", guild_id)
                        #ctx.send(chan, user_msg)

if __name__ == "__main__":
    api = os.getenv("API_SECRET")
    irc_thread = Thread(target=irc_bot.start)
    bot.run(api)
    #irc_bot.start()
    #bot.run(api)



