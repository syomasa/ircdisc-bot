import json
from discord.ext import commands


class Server(commands.Bot):
    def __init__(self, *args, **kvargs):
        super().__init__(*args, **kvargs)
        with open("config.json", "r") as file:
            self.config = json.load(file)

    def print_guilds(self):
        print(self.guilds)

    def print_irc_channels(self):
        for i, guild in enumerate(self.guilds):
            print(self.config["servers"][i]["channel"])

    def update_config(self):
        print("Config updated")
        with open("config.json", "r") as file:
            self.config = json.load(file)

    def set_irc_channels(self, listener):
        for i, guild in enumerate(self.guilds):
            listener.channels.append(self.config["servers"][i]["channel"])
            listener.join(self.config["servers"][i]["channel"])
        print(listener.channels)

    def get_guild_id(self, channel):
        for i in self.config["servers"]:
            if channel == i["channel"]:
                return i["guild"]

    def get_channel_name(self, guild_id):
        for i in self.config["servers"]:
            if guild_id == int(i["guild"]):
                return i["channel"]

class Sender(commands.Bot):

    def __init__(self, config, *args, **kvargs):
        super().__init__(*args, **kvargs)
        with open(config, "r") as file:
            self.config = json.load(file)

    def get_guilds(self):
        print(self.guilds)
        return self.guilds

    def test_config(self):
        print(self.config)
