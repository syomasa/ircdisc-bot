import socket
import dotenv
import os
import re
import socks

dotenv.load_dotenv()

nick = os.getenv("NICK")
server = os.getenv("SERVER")
port = os.getenv("PORT")
ident = os.getenv("IDENT")
realname= os.getenv("REALNAME")
chan = os.getenv("CHAN")

proxy_server = os.getenv("PROXY_SERVER")
proxy_port = os.getenv("PROXY_PORT")

class Bot:
    def __init__(self, server,  nick, ident, realname, proxy=False, port=6667):

        if proxy==True:
            self.s = socks.socksocket()
            self.s.set_proxy(socks.SOCKS5, proxy_server, int(proxy_port))
        else:
            self.s = socket.socket()
        self.s.connect((server, int(port)))
        self.s.send(("NICK %s\r\n" % nick).encode())
        self.s.send(("USER %s %s bla :%s\r\n" % (ident, server, realname)).encode())

        self.server = server
        self.nick = nick
        self.ident = ident
        self.realname = realname
        self.parse_func = None
        self.channels = [] # list of channels where bot have joined

    def join(self, chan):
        self.s.send(f"JOIN :{chan}\r\n".encode())
        self.channels.append(chan)

    def send(self, chan, msg):
        if chan in self.channels:
            self.s.send(("PRIVMSG %s :%s\r\n" % (chan, msg)).encode("ISO-8859-1"))

    def receive_msg(self):
        msg = self.s.recv(2040)
        return msg.decode()

    def start(self):
        self.parse_func(self)

    def send_to_disc(msg:str, channel):
        channel.send(msg)


    def __call__(self, func):
        """Used as main loop of bot"""
        self.parse_func = func




if __name__ == "__main__":
    bot = Bot(server, nick, ident, realname)
    bot.join(chan)
    #bot.send(chan, "Howdy partner")

    @bot
    def parse(ctx):
        while 1:
            msg = bot.receive_msg()
            print(msg)

            lines = msg.split("\r\n")
            for line in lines:
                parts = line.split(":")
                #print(parts[0].encode())
                if parts[0].strip() == "PING":
                    text = f"PONG :{parts[1]}\r\n"
                    ctx.s.send(text.encode())
                    print(f"Msg sent: {text}")
                else:
                    # I trust past me on this one :D
                    pattern =  re.match("(:.+?(?=!)).*?([A-Z].*)", line, re.M)
                    if pattern == None:
                        continue

                    groups = pattern.groups()
                    username = groups[0].strip(":")
                    split_command = groups[1].split(" ")
                    print(split_command)
                    print(split_command[0:2])

                    try:
                        if("PRIVMSG" not in split_command[0:2]):
                            continue
                    except IndexError:
                           continue
                    else:
                        user_msg = ' '.join(split_command[2:])[1:]
                        print(f"<{username}> {user_msg}")
